from typing import List
from pydantic import BaseModel


class DataFrame(BaseModel):
    data: List[float]
