Project status
It's a pet project for training skills in CI/CD DS Course
https://ods.ai/tracks/ml-in-production-spring-23

Технологический стек
* Язык разработки: Python 3.9
* Менеджмент зависимостей: pip
* Менеджер виртуальных окружений: venv
* Шаблон проекта: cookiecutter-data-science
* Cистема контроля версий кода – gitlab
* Cистема контроля версий данных – dvc
* workflow менеджер + контроль версии данных – dvc
* Трекинг экспериментов – mlflow
* Сервер/ Клиент хранилище данных - minio
* Инструмент контроля codestyle – flake8
* Тестирование: unittest
* CLI: click
* Модель – LSTM
* Api – FastAPI
* Runtime – docker

Installation

* Install python 3.9
* git clone https://gitlab.com/galandrey/mlops.git
* python -m venv venv
* venv/Scripts/activate
* python.exe -m pip install --upgrade pip
* pip install -r requirements.txt

Setting credential 
* .dvc/config (copy from .dvc/config_example) 
* .env (copy from .env_example)

Install docker
Start dockers
* docker-compose -d up --build

Starting DVC
* dvc pull
* dvc repro

Start mlflow
>mlflow server --backend-store-uri postgresql://root:root@127.0.0.1:5432/test_db --default-artifact-root s3://arts --host 0.0.0.0 --no-serve-artifacts
>mlflow models serve --no-conda -m s3://arts/5/ade50d4748eb4b5da3f06833ea94db00/artifacts/my_model -h 0.0.0.0 -p 8001
 
Start inference api (FastAPI+mlflowModel)
* cd ./api
* docker-compose -d up --build
