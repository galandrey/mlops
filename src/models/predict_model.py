import sys
import os
import click
import logging
import pandas as pd
import torch

# insert project folder to PATH
sys.path.insert(1, os.path.join(sys.path[0], '../..'))
from src.config_params import config
from src.models.lstm_model import LSTMModel
from src.data.data_process import data_prepare_eval, get_scaler_fitted


def predict(model, data_in, scaler):
    data_x = data_prepare_eval(data_in, scaler)
    model.eval()
    prediction = model(data_x)
    prediction = prediction.cpu().detach().numpy()
    data_pred = scaler.inverse_transform(prediction.reshape(-1, 1)).flatten()
    return data_pred


@click.command()
@click.argument('model_save_filepath', type=click.Path(exists=True))
@click.argument('data_train_filepath', type=click.Path(exists=True))
@click.argument('data_test_filepath', type=click.Path(exists=True))
@click.argument('data_predict_filepath', type=click.Path())
def main(model_save_filepath, data_train_filepath, data_test_filepath, data_predict_filepath):
    logger = logging.getLogger(__name__)
    logger.info("train model =====================>")
    logger.info(f"model_save_filepath='{model_save_filepath}', "
                f"data_train_filepath='{data_train_filepath}', "
                f"data_test_filepath='{data_test_filepath}' "
                f"data_predict_filepath='{data_predict_filepath}'"
                )

    model = LSTMModel(input_size=config["model"]["input_size"], hidden_layer_size=config["model"]["lstm_size"],
                      num_layers=config["model"]["num_lstm_layers"], output_size=1, dropout=config["model"]["dropout"])
    model.load_state_dict(torch.load(model_save_filepath))
    model = model.to(config["training"]["device"])

    data_train = pd.read_csv(data_train_filepath, sep='\t').to_numpy()
    data_test = pd.read_csv(data_test_filepath, sep='\t').to_numpy()

    scaler = get_scaler_fitted(data_train)
    data_test_x = data_prepare_eval(data_test, scaler)
    data_test_pred = predict(model, data_test_x, scaler)

    pd.DataFrame(data_test_pred).to_csv(data_predict_filepath, sep='\t', index=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
