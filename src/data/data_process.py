import numpy as np
import torch
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from sklearn.preprocessing import StandardScaler

from src.config_params import config


class TimeSeriesDataset(Dataset):
    def __init__(self, x, y):
        # in our case, we have only 1 feature, so we need to convert `x` into [batch, sequence, features] for LSTM
        x = np.expand_dims(x, 2)
        self.x = x.astype(np.float32)
        self.y = y.astype(np.float32)

    def __len__(self):
        return len(self.x)

    def __getitem__(self, idx):
        return (self.x[idx], self.y[idx])


def get_x(data_in, window_size):
    rows_num = data_in.shape[0]
    data_x = []
    for i in range(rows_num - window_size):
        data_x.append(data_in[i:i + window_size])
    return np.array(data_x)


def get_y(data_in, window_size):
    return data_in[window_size:]


def get_scaler_fitted(data_in):
    scaler = StandardScaler()
    data_in_2d = data_in.reshape(-1, 1)
    scaler.fit(data_in_2d)
    return scaler


def data_prepare_train(data_in, scaler, shuffle=False):
    data_2d = data_in.reshape(-1, 1)
    data_scaled = scaler.transform(data_2d)
    data_scaled = data_scaled.flatten()

    data_x = get_x(data_scaled, config["data"]["window_size"])
    data_y = get_y(data_scaled, config["data"]["window_size"])
    print("Data shape:", data_x.shape, data_y.shape)

    data_set = TimeSeriesDataset(data_x, data_y)
    data_loader = DataLoader(data_set, batch_size=config["training"]["batch_size"], shuffle=shuffle)

    return data_loader


def data_prepare_eval(data_in, scaler):
    data_2d = data_in.reshape(-1, 1)
    data_scaled = scaler.transform(data_2d)
    data_scaled = data_scaled.flatten()

    data_x = get_x(data_scaled, config["data"]["window_size"])
    data_x_tensor = torch.tensor(data_x).float().to(config["training"]["device"]).unsqueeze(
        2)  # [batch, sequence, feature]

    return data_x_tensor
