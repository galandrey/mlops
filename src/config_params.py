config = {
    "data": {
        "window_size": 64,
        "train_split_part": 0.80,
        "test_split_size": 10 + 64,
    },
    "model": {
        "input_size": 1,    # since we are only using 1 feature, close price
        "num_lstm_layers": 2,
        "lstm_size": 32,
        "dropout": 0.2,
    },
    "training": {
        "device": "cpu",    # "cuda" or "cpu"
        "batch_size": 32,
        "num_epoch": 10,
        "learning_rate": 0.01,
        "scheduler_step_size": 25,
        "scheduler_step_gamma": 0.3,
    }
}
