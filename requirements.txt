numpy
pandas
matplotlib
notebook
torch
torchsummary
scikit-learn
flake8
fsspec==2022.11.0 # for dvc, skip warning
dvc[s3]
click
python-dotenv
uvicorn
fastapi
mlflow==2.3.2



