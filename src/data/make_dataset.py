import sys
import os
import click
import logging
# from dotenv import find_dotenv, load_dotenv
import pandas as pd

# insert project folder to PATH
sys.path.insert(1, os.path.join(sys.path[0], '../..'))
from src.config_params import config


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('data_train_filepath', type=click.Path())
@click.argument('data_val_filepath', type=click.Path())
@click.argument('data_test_filepath', type=click.Path())
def main(input_filepath, data_train_filepath, data_val_filepath, data_test_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("make dataset ======================>")
    logger.info(f"input_filepath='{input_filepath}', "
                f"data_train_filepath='{data_train_filepath}', "
                f"data_val_filepath='{data_val_filepath}' "
                f"data_test_filepath='{data_test_filepath}'"
                )

    data = pd.read_csv(input_filepath, sep='\t')
    print("data len import", len(data))
    data = data[0:10000]
    print("data len set", len(data))
    print(data.head())

    data.rename(columns={'<DATE>': 'DATE', '<TIME>': 'TIME',
                         '<OPEN>': 'OPEN', '<HIGH>': 'HIGH', '<LOW>': 'LOW', '<CLOSE>': 'CLOSE',
                         '<TICKVOL>': 'TICKVOL', '<VOL>': 'VOL', '<SPREAD>': 'SPREAD'},
                inplace=True
                )

    train_size = int(len(data) * config["data"]["train_split_part"])
    test_size = config["data"]["test_split_size"]

    data_train = data[:train_size]
    data_val = data[train_size:-test_size]
    data_test = data[-test_size:]

    columns_csv = ["CLOSE"]
    data_train.to_csv(data_train_filepath, sep='\t', columns=columns_csv, index=False)
    data_val.to_csv(data_val_filepath, sep='\t', columns=columns_csv, index=False)
    data_test.to_csv(data_test_filepath, sep='\t', columns=columns_csv, index=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    # load_dotenv(find_dotenv())

    main()
