import unittest
from src.visualization.calc import *

class CalcTest(unittest.TestCase):
    def test_plus(self):
        self.assertEqual(plus(2, 2), 4)

    def test_minus(self):
        self.assertEqual(minus(6, 2), 4)
    def test_mult(self):
        self.assertEqual(mult(6, 0), 0)

    def test_dev(self):
        self.assertEqual(devide(6, 2), 3)

    def test_dev_by_zero(self):
        with self.assertRaises(ValueError) as e:
            devide(6, 0)


if __name__ == '__main__':
    unittest.main()
