from fastapi import FastAPI
from schemas import DataFrame
from dotenv import load_dotenv, find_dotenv
from model import ModelMLFLOW
from sklearn.preprocessing import StandardScaler

# Load environments
load_dotenv(find_dotenv())

# Init new FastAPI application
app = FastAPI()

# Create model
model = ModelMLFLOW("lstm_model1", "Staging")

# set mean and scale jast for example
scaler = StandardScaler()
scaler.mean_ = [0.5, 0.5]
scaler.scale_ = [1, 1]
res = scaler.transform([[1, 1]])


@app.get("/")
async def main():
    return {"message": "Use method /predict to inference"}


@app.post("/predict")
async def predict(data: DataFrame):
    data_scaled = scaler.transform([data.data])
    print("data_scaled", data_scaled)
    prediction = model.predict()
    print("prediction", prediction)

    return prediction
