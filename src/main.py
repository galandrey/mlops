import pandas as pd
import matplotlib.pyplot as plt
import torch

from src.models.lstm_model import LSTMModel
from src.models.train_model import train_model
from src.models.predict_model import predict
from src.data.data_process import data_prepare_train, data_prepare_eval, get_scaler_fitted

from src.config_params import config

print("config", config)

data = pd.read_csv("../data/raw/RTS Splice_M5.csv", sep='\t')
print("data len import", len(data))
data = data[0:10000]
print("data len", len(data))
print(data.head())

data.rename(
    columns={'<DATE>': 'DATE', '<TIME>': 'TIME', '<OPEN>': 'OPEN', '<HIGH>': 'HIGH', '<LOW>': 'LOW', '<CLOSE>': 'CLOSE',
             '<TICKVOL>': 'TICKVOL', '<VOL>': 'VOL', '<SPREAD>': 'SPREAD'}, inplace=True)

# plot data
fig = plt.figure(figsize=(25, 5), dpi=80)
plt.plot(data["CLOSE"])
plt.title("RTS M5")
plt.show()

data_np = data["CLOSE"].to_numpy()

train_size = int(len(data_np) * config["data"]["train_split_part"])
test_size = config["data"]["test_split_size"]

data_train = data_np[:train_size]
data_val = data_np[train_size:-test_size]

scaler = get_scaler_fitted(data_train)
dataloader_train = data_prepare_train(data_train, scaler, shuffle=True)
dataloader_val = data_prepare_train(data_val, scaler, shuffle=False)

model = LSTMModel(input_size=config["model"]["input_size"], hidden_layer_size=config["model"]["lstm_size"],
                  num_layers=config["model"]["num_lstm_layers"], output_size=1, dropout=config["model"]["dropout"])
model = model.to(config["training"]["device"])

train_model(model, dataloader_train, dataloader_val)

# save model
torch.save(model.state_dict(), "../models/LSTMModel_weights.pth")

# load model
# model = LSTMModel(input_size=config["model"]["input_size"], hidden_layer_size=config["model"]["lstm_size"],
#                   num_layers=config["model"]["num_lstm_layers"], output_size=1, dropout=config["model"]["dropout"])
# model.load_state_dict(torch.load("../models/LSTMModel_weights.pth"))

data_test = data_np[-test_size:]
data_test_x = data_prepare_eval(data_test, scaler)
data_test_pred = predict(model, data_test_x, scaler)

# plot data target & prediction
prediction_size = len(data_test_pred)
plt.figure(figsize=(25, 5), dpi=80)
plt.plot(data_test[-prediction_size:], label="target price")
plt.plot(data_test_pred, label="prediction price")
plt.title("RTS M5: target / prediction")
plt.show()
