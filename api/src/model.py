import mlflow
import numpy as np


class ModelMLFLOW:
    def __init__(self, model_name: str, model_stage: str):
        """
        Initialize the model
        :param model_name: model name in mlflow registry
        :param model_stage: model stage in mlflow registry
        """
        # load model from mlflow registry
        self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")

    def predict(self, data_in: np.ndarray) -> np.ndarray:
        """
        Make predictions on input data
        :param data_in:
        :return: predicted data
        """
        return self.model.predict(data_in)
