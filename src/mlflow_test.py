import os
import mlflow
from mlflow.models.signature import infer_signature
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_diabetes
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

mlflow_tracking_uri = os.getenv('MLFLOW_TRACKING_URI')
mlflow.set_tracking_uri(mlflow_tracking_uri)
mlflow.set_experiment("Andrey_timeweb")

with mlflow.start_run():
    db = load_diabetes()
    X_train, X_test, y_train, y_test = train_test_split(db.data, db.target)

    print(X_test[0:2])

    # Create and train models.
    rf = RandomForestRegressor(n_estimators=100, max_depth=6, max_features=3)
    rf.fit(X_train, y_train)

    # Use the model to make predictions on the test dataset.
    predictions = rf.predict(X_test)

    mlflow.log_param('n_estimators', 100)
    mlflow.log_param('max_depth', 6)
    mlflow.log_param('max_features', 3)

    metrics = {'mse': mean_squared_error(y_test, predictions),
               'r2_score': r2_score(y_test, predictions)
               }
    mlflow.log_metrics(metrics)

    signature = infer_signature(X_test, predictions)
    mlflow.sklearn.log_model(rf, "my_model", signature=signature)
