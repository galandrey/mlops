import sys
import os
import click
import logging
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim

import mlflow
from dotenv import load_dotenv, find_dotenv

# insert project folder to PATH
sys.path.insert(1, os.path.join(sys.path[0], '../..'))
from src.config_params import config
from src.models.lstm_model import LSTMModel
from src.data.data_process import data_prepare_train, get_scaler_fitted


def run_epoch(model, dataloader, optimizer, criterion, scheduler, is_training=False, ):
    epoch_loss = 0

    if is_training:
        model.train()
    else:
        model.eval()

    for idx, (x, y) in enumerate(dataloader):
        if is_training:
            optimizer.zero_grad()

        batchsize = x.shape[0]

        x = x.to(config["training"]["device"])
        y = y.to(config["training"]["device"])

        out = model(x)
        loss = criterion(out.contiguous(), y.contiguous())

        if is_training:
            loss.backward()
            optimizer.step()

        epoch_loss += (loss.detach().item() / batchsize)

    lr = scheduler.get_last_lr()[0]

    return epoch_loss, lr


def train_model(model, dataloader_train, dataloader_val, model_save_filepath=""):
    # define optimizer, scheduler and loss function
    print("\nTrain model:")
    optimizer = optim.Adam(model.parameters(), lr=config["training"]["learning_rate"], betas=(0.9, 0.98), eps=1e-9)
    criterion = nn.MSELoss()
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=config["training"]["scheduler_step_size"],
                                          gamma=config["training"]["scheduler_step_gamma"])

    # begin training
    metrics = {}
    for epoch in range(config["training"]["num_epoch"]):
        loss_train, lr_train = run_epoch(model, dataloader_train, optimizer, criterion, scheduler, is_training=True)
        loss_val, lr_val = run_epoch(model, dataloader_val, optimizer, criterion, scheduler)
        scheduler.step()

        print('Epoch[{}/{}] | loss train:{:.6f}, test:{:.6f} | lr:{:.6f}'
              .format(epoch + 1, config["training"]["num_epoch"], loss_train, loss_val, lr_train))
        metrics = {"loss_train": loss_train, "loss_val": loss_val}

    return metrics


@click.command()
@click.argument('data_train_filepath', type=click.Path(exists=True))
@click.argument('data_val_filepath', type=click.Path(exists=True))
@click.argument('model_save_filepath', type=click.Path())
def main(data_train_filepath, data_val_filepath, model_save_filepath):
    logger = logging.getLogger(__name__)
    logger.info("train model =====================>")
    logger.info(f"data_train_filepath='{data_train_filepath}', "
                f"data_val_filepath='{data_val_filepath}' "
                f"model_save_filepath='{model_save_filepath}'"
                )

    mlflow.log_param('input_size', config["model"]["input_size"])
    mlflow.log_param('num_lstm_layers', config["model"]["num_lstm_layers"])
    mlflow.log_param('lstm_size', config["model"]["lstm_size"])
    mlflow.log_param('output_size', 1)

    mlflow.log_param('batch_size', config["training"]["batch_size"])
    mlflow.log_param('num_epoch', config["training"]["num_epoch"])
    mlflow.log_param('learning_rate', config["training"]["learning_rate"])
    mlflow.log_param('scheduler_step_size', config["training"]["scheduler_step_size"])
    mlflow.log_param('scheduler_step_gamma', config["training"]["scheduler_step_gamma"])

    data_train = pd.read_csv(data_train_filepath, sep='\t').to_numpy()
    data_val = pd.read_csv(data_val_filepath, sep='\t').to_numpy()

    scaler = get_scaler_fitted(data_train)
    dataloader_train = data_prepare_train(data_train, scaler, shuffle=True)
    dataloader_val = data_prepare_train(data_val, scaler, shuffle=False)

    model = LSTMModel(input_size=config["model"]["input_size"], hidden_layer_size=config["model"]["lstm_size"],
                      num_layers=config["model"]["num_lstm_layers"], output_size=1, dropout=config["model"]["dropout"])
    model = model.to(config["training"]["device"])

    train_metrics = train_model(model, dataloader_train, dataloader_val)
    mlflow.log_metrics(train_metrics)

    # save model
    if model_save_filepath == "":
        model_save_filepath = "../models/Model_weights.pth"
    torch.save(model.state_dict(), model_save_filepath)

    mlflow.pytorch.log_model(model, "lstm_model")
    # mlflow.log_artifact("model", "../models/LSTMModel_weights.pth")


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    load_dotenv(find_dotenv())

    mlflow_tracking_uri = os.getenv('MLFLOW_TRACKING_URI')
    mlflow.set_tracking_uri(mlflow_tracking_uri)
    mlflow.set_experiment("lstm")

    with mlflow.start_run():
        main()
